
import java.util.*;

public class Node {
   // parsePostFix'i jaoks ei kasutanud yhtegi välisallikat.
   // Meetodid setNextSibling, addSibling ja hasNext on võetud loengu materjalidest.
   // Meetodile leftParentheticRepresentation on lahenduse idee allikas viidatud.
   private String name;
   private Node firstChild;
   private Node nextSibling;

   Node (String n, Node d, Node r) {
      this.name = n;
      this.firstChild = d;
      this.nextSibling = r;
   }
   Node (String n) {
      this.name = n;
      this.firstChild = null;
      this.nextSibling = null;
   }

   public void setNextSibling (Node p) {
      nextSibling = p;
   }

   public boolean hasNext() {
      return (nextSibling != null);
   }

   public void addSibling (Node a) {
      if (a == null) return;
      Node sibling = nextSibling;
      if (sibling == null)
         setNextSibling(a);
      else {
         while (sibling.hasNext()) {
            sibling = sibling.nextSibling;
         }
         sibling.setNextSibling(a);
      }
   }

   public static Node parsePostfix (String s) {
      Stack<Node> stack = new Stack<>();
      Node root;

      List<String> elements = normal(s);
      boolean parentInc = false;
      boolean siblingInc = false;
      boolean newSibling = false;
      for (int i = 0; i<elements.size(); i++) {
         if (elements.get(i).equals("(")){
            if(siblingInc){
               siblingInc = false;
               newSibling = true;
            }
         }else if (elements.get(i).equals(",")){
            if (siblingInc){
               throw new RuntimeException("Wrong input string, can not have 2 commas in sucession:" + s);
            }
            siblingInc = true;
         }else if (elements.get(i).equals(")")){
            if (parentInc){
               throw new RuntimeException("Wrong input string:" + s);
            }
            parentInc = true;
         }else{
            if (!siblingInc && !parentInc && !newSibling){
               root = new Node(elements.get(i));
               stack.push(root);
            }else if (siblingInc){
               if (elements.get(i+1).equals("(")){
                  newSibling = true;
                  siblingInc = false;
               }else {
                  Node prev = stack.pop();
                  prev.addSibling(new Node(elements.get(i)));
                  stack.push(prev);
                  siblingInc = false;
               }
            }else if (parentInc){
               Node prev = stack.pop();
               if (stack.size() == 1){
                  Node par = new Node(elements.get(i));
                  par.firstChild = prev;
                  Node fChild = stack.pop();
                  fChild.addSibling(par);
                  stack.push(fChild);
                  parentInc = false;
               }else{
                  Node parent = new Node(elements.get(i));
                  parent.firstChild = prev;
                  stack.push(parent);
                  parentInc = false;
               }
            }else if (newSibling){
               Node newSib = new Node(elements.get(i));
               stack.push(newSib);
               newSibling = false;
            }
         }
      }
      if (stack.size() != 1){
         throw new RuntimeException("Wrong input string: " + s);
      }
      Node treeRoot = stack.pop();
      if(treeRoot.nextSibling != null){
         throw new RuntimeException("Root is not presented in string " + s);
      }
      return treeRoot;
   }

   public static List<String> normal(String s){
      String[] unWanted = {"\t", "\n"};
      List<String> elements = new ArrayList<>();
      char[] chList = s.toCharArray();
      StringBuilder Current = new StringBuilder();
      int rightCount = 0;
      int leftCount = 0;
      int totalSymbols = 0;
      int treeElements = 0;
      for (Character ch: chList) {
         if(rightCount > leftCount){
            throw new RuntimeException("Wrong input string, check your brackets: " +s);
         }
         if(ch.toString().equals(")")){
            rightCount ++;
         }
         if(ch.toString().equals("(")){
            leftCount ++;
         }
         if (ch.toString().equals(")") || ch.toString().equals("(") || ch.toString().equals(",")){
            if(!Current.toString().equals("")){
               elements.add(Current.toString());
               Current = new StringBuilder();
            }
            elements.add(ch.toString());
            totalSymbols ++;
         }else{
            if (!ch.toString().equals(" ")){
               Current.append(ch);
               treeElements ++;
            }else{
               elements.add(Current.toString());
               Current = new StringBuilder();

            }
         }
      }
      if(!Current.toString().equals("") && !Arrays.asList(unWanted).contains(Current.toString())){
         elements.add(Current.toString());
      }
      if(elements.size() == 0 || (totalSymbols == 0 && elements.size() != 1) || (totalSymbols !=0 && treeElements == 0)){
         throw new RuntimeException("Invalid input string: " + s);
      }
      return elements;
   }

   public String leftParentheticRepresentation(){
      //http://www.sr2jr.com/textbook-solutions/computer-science/71801009/building-java-programs-a-back-to-basics-approach-binary-trees#

      StringBuilder result = new StringBuilder();

      if (name == null){
         return result.toString();
      } else {
         result.append(name);

         if (firstChild != null && nextSibling != null) {
            result.append("(").append(firstChild.leftParentheticRepresentation()).append(")").append(",").append(nextSibling.leftParentheticRepresentation());
         }else if (firstChild != null){
            result.append("(").append(firstChild.leftParentheticRepresentation()).append(")");
         }else if (nextSibling != null){
            result.append(",").append(nextSibling.leftParentheticRepresentation());
         }
      }
      return result.toString();
   }
   public String treeToXML(int... depth){
      int treeDepth = depth.length > 0 ? depth[0] : 1;
      StringBuilder result = new StringBuilder();
      StringBuilder tabs = new StringBuilder();
      tabs.append("\t".repeat(treeDepth - 1));

      String startTag = tabs.toString() + "<L" + treeDepth + "> " + name;
      String endTag = tabs.toString() + "</L" + treeDepth + ">";
      String endTag1 = " </L" + treeDepth + ">";

      if (name == null){
         return result.toString();
      } else {
         result.append(startTag);
         if (firstChild != null && nextSibling != null) {
            result.append("\n")
                    .append(firstChild.treeToXML(treeDepth + 1))
                    .append("\n")
                    .append(endTag)
                    .append("\n")
                    .append(nextSibling.treeToXML(treeDepth));
         }else if (firstChild != null){
            result.append("\n")
                    .append(firstChild.treeToXML(treeDepth + 1))
                    .append("\n")
                    .append(endTag);
         }else if (nextSibling != null){
            result.append(endTag1)
                    .append("\n")
                    .append(nextSibling.treeToXML(treeDepth));
         }else{
            result.append(endTag1);
         }
      }
      return result.toString();
   }


   public static void main (String[] param) {
      String s = "ABC";
      Node t = Node.parsePostfix (s);
      System.out.println(t.treeToXML());

   }
}

